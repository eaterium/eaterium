package com.example.jah.gridtest;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ImageView;

/**
 * Created by jah on 6/9/2017.
 */

public class DynameHeightlmageView extends ImageView {

    private  float whRatio = 0;

    public DynameHeightlmageView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }


    public DynameHeightlmageView(Context context) {
        super(context);
    }
    public void setRatio(float ratio) {
         whRatio = ratio;
    }
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        if (whRatio != 0) {
            int width = getMeasuredWidth();
            int height = (int)(whRatio * width);
            setMeasuredDimension(width, height);
        }
    }
}
