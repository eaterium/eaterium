package com.example.jah.gridtest;

import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;

import java.util.List;

/**
 * Created by jah on 6/9/2017.
 */

public class PlaceHolderDrawableHelper {
    static final  String placeholderValues[] ={
            "#76A599", "#7D9A96", "#6F8888",
            "#82A1AB", "#8094A1", "#74838C",
            "#B6A688", "#9E9481", "#8D8F9F",
            "#A68C93", "#968388", "#7E808B"};
    static List<Drawable> drawableBackgroundList;

    public static Drawable getdrawableBackgroundList (int position){
        if (drawableBackgroundList == null ||drawableBackgroundList.size()==0){
            for (int i=0; i<placeholderValues.length; i++){
                int color = Color.parseColor(placeholderValues[i]);
                drawableBackgroundList.add(new ColorDrawable(color));
            }
        }
        return drawableBackgroundList.get(position % placeholderValues.length);
    }



}
