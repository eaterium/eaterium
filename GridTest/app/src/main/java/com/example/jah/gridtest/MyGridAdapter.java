package com.example.jah.gridtest;

import android.animation.AnimatorSet;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by jah on 6/9/2017.
 */

public class MyGridAdapter extends RecyclerView.Adapter {

    private Context mContext;
    private List<ImageModel> imageModels = new ArrayList<>();

    public MyGridAdapter(Context context){
        mContext = context;
    }
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int position) {
        View itemView = LayoutInflater.from(mContext).inflate(R.layout.resizable_grid_item, null);
        MyViewHolder holder = new MyViewHolder(itemView);
        holder.imageView = (DynameHeightlmageView) itemView.findViewById(R.id.dynamic_height_image_view);
        holder.positionTextView = (TextView) itemView.findViewById(R.id.item_position_view);
        itemView.setTag(holder);
        return holder;
    }
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int position) {

        MyViewHolder vh = (MyViewHolder) viewHolder;
        ImageModel item = imageModels.get(position);
        RelativeLayout.LayoutParams rlp = (RelativeLayout.LayoutParams) vh.imageView.getLayoutParams();
        float ratio = item.getHeight() / item.getWidth();
        rlp.height = (int) (rlp.width * ratio);
        vh.imageView.setLayoutParams(rlp);
        vh.positionTextView.setText("pos: " + position);
        vh.imageView.setRatio(item.getRatio());
        Picasso.with(mContext).load(item.getUrl()).placeholder(PlaceHolderDrawableHelper.getdrawableBackgroundList(position)).into(vh.imageView);
    }
    public int getItemCount() {
        return imageModels.size();
    }

    public void addDrawable(ImageModel imageModel) {
        float ratio = (float) imageModel.getHeight() / (float) imageModel.getWidth();
        imageModel.setRatio(ratio);
        this.imageModels.add(imageModel);
    }


}
