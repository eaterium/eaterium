package com.example.jah.gridtest;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

/**
 * Created by jah on 6/9/2017.
 */

public class MyViewHolder extends RecyclerView.ViewHolder {

    public DynameHeightlmageView imageView;
    public TextView positionTextView;
    public MyViewHolder(View itemView) {
        super(itemView);
    }
}
