package com.example.jah.gridtest;

/**
 * Created by jah on 6/9/2017.
 */

public class ImageModel {
    private int width;
    private int height;
    private float ratio;
    private String url;

    public int getWidth() {
        return  width;}

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }
    public void setHeight(int height) {
        this.height = height;
    }

    public float getRatio() {
        return ratio;
    }

    public void setRatio(float ratio) {
        this.ratio = ratio;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

}
