package com.example.jah.gridtest;

import android.os.AsyncTask;

import java.io.BufferedReader;
import java.io.InputStream;

/**
 * Created by jah on 6/9/2017.
 */

 class LoadFlickerFeed extends AsyncTask<String, Void,String[]> {
    private static final String JSON_FLICKR_FEED_START = "jsonFlickrFeed({";
    private static final CharSequence JSON_FLICKR_FEED_END = "})";
    @Override
    protected String[] doInBackground(String... params) {
        String tags = "";
        for (String param : params){
            if (tags.length() == 0) {
                tags = param;
            }else {
                tags = "," + param;
            }
        }
        return readUrl (String.format("https://api.flickr.com/services/feeds/photos_public.gne?format=json&tags=%s", tags));
    }
    private String[] readUrl(String urlString){
        InputStream in = null;
        BufferedReader reader=null;

    }

}
